﻿using System.ComponentModel.DataAnnotations;

namespace EventosAPI.Entidades
{
    public class Usuario
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "El campo nombre es requerido")]
        public string Nombre { get; set; }

        public string Correo { get; set; }

        //BD donde se guarda el id del evento
        public int EventoId { get; set; }
        //Obtiene la informacion del evento relacionada al usuario
        public Evento evento { get; set; }
        public int OrganizadorId { get; set; }
        public Organizador organizador { get; set; }

        public List<Evento> Eventos { get; set; }

        public List<Organizador> Organizadores { get; set; }
    }
}
