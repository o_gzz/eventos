﻿namespace EventosAPI.Entidades
{
    public class Organizador
    {
        public int Id { get; set; }
        public string Nombre { get; set; }

        public string Comentarios { get; set; }

        public int EventoId { get; set; }
        //Obtiene la informacion del evento relacionada al usuario
        public Evento evento { get; set; }


        public List<Evento> Eventos { get; set; }
        public List<Usuario> Usuarios { get; set; }

    }
}
