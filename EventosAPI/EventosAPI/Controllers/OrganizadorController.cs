﻿using EventosAPI.Entidades;
using EventosAPI.Servicios;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace EventosAPI.Controllers
{
    [ApiController]
    [Route("api/organizadores")]
    public class OrganizadorController: ControllerBase
    {
        //Context
        public readonly ApplicationDbContext _context;
        //Constructor
        public OrganizadorController(ApplicationDbContext context)
        {
            _context = context;
        }
        //Servicio
        public OrganizadoresServicio _organizadoresServicio;
        public OrganizadorController(OrganizadoresServicio organizadoresServicio)
        {
            _organizadoresServicio = organizadoresServicio;

        }
        

        //Guardar
        [HttpPost("agregar-organizador")]
        public async Task<ActionResult> Post(Organizador organizador) //Espera un JSON con los atributos que tiene el evento
        {
            _context.Add(organizador); //Guardado hasta el contexto
            await _context.SaveChangesAsync(); //guardar de manera ascincrona
            return Ok();//Sin errores
        }

        //Obtener todos los eventos                             Include(x => x.Usuarios).ToListAsync();
        [HttpGet("lista-organizadores")]
        public async Task<ActionResult<List<Organizador>>> GetAll()
        {
            return await _context.Organizadores.ToListAsync();
        }


        //Actualizar
        //string para nombre
        [HttpPut("{id:int}")]
        //Validacion para saber si existe
        public async Task<ActionResult> Put(Evento evento, int id)
        {
            if (evento.Id != id)
            {
                return BadRequest("El id no coincide con el de la URL");
            }

            _context.Update(evento);
            await _context.SaveChangesAsync();
            return Ok();
        }

        //Eliminar
        [HttpDelete("{id:int}")]
        //Cualquier id que coincida
        public async Task<ActionResult> Delete(int id)
        {
            var exist = await _context.Eventos.AnyAsync(x => x.Id == id);
            if (!exist)
            {
                return NotFound("No se encontro elemento en la base de datos");
            }
            //Obj a enviar es instancia que solo tenga el valor de id
            _context.Remove(new Evento() { Id = id });
            await _context.SaveChangesAsync();
            return Ok();

        }
    }
}
