﻿using EventosAPI.Entidades;
using EventosAPI.ViewModels;

namespace EventosAPI.Servicios
{
    public class OrganizadoresServicio
    {
        private ApplicationDbContext _context;
        //Constructor
        public OrganizadoresServicio(ApplicationDbContext context)
        {
            _context = context;
        }

        public Organizador GetOrganizadorById(int organizadorId) => _context.Organizadores.FirstOrDefault(n => n.Id == organizadorId);
        public List<Organizador> GetAllOrganizadores() => _context.Organizadores.ToList();

        
    }
}
