﻿using EventosAPI.Entidades;
using EventosAPI.ViewModels;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace EventosAPI.Servicios
{
    public class EventosServicio
    {
        private ApplicationDbContext _context; 
        public EventosServicio(ApplicationDbContext context) 
        {
            _context = context;
        }
        /*
        public void AgregarEvento (EventoVM evento)
        {
            var _evento = new Evento()
            {
                Nombre = evento.Nombre,
                Fecha = DateTime.Now,
                Ubicacion = evento.Ubicacion
            };
            _context.Eventos.Add(_evento); //guardar de manera ascincrona
            _context.SaveChanges();
        }
        */
        public List<Evento> GetAllEventos() => _context.Eventos.ToList();

        
 
    }
}
